package com.mycompany.algorithmdesign_hash.LinkedList;

public class HashLinkedList {

    int M; // Capacity
    int size; // Number of key-values pairs
    Node[] list;

    public HashLinkedList() {
        this.M = 11; // Default constructor
        list = new Node[M];
        this.size = 0;
    }

    public HashLinkedList(int M) {
        this.M = M;
        list = new Node[M];
        this.size = 0;
    }

    class Node {

        int key;
        String value;
        Node next; // Reference to the next Node

        Node(int key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    private int hashFunc(int key) {
        int h = key % M;
        return h;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public String get(int key) {
        int h = hashFunc(key);
        for (Node i = list[h]; i != null; i = i.next) {
            if (key == i.key) {
                return i.value;
            }
        }
        return null;
    }

    public void getAll() {
        for (int i = 0; i < M; i++) {
            for (Node j = list[i]; j != null; j = j.next) {
                System.out.println(i + " -> " + j.key + " " + j.value + " this .next is : "+j.next);
            }
        }
    }

    public void put(int key, String value) {
        int h = hashFunc(key);
        Node head = list[h];
        while (head != null) {
            if (head.key == key) {
                head.value = value;
                return;
            }
            head = head.next;
        }
        size++;
        head = list[h];
        Node node = new Node(key, value);
        node.next = head;
        list[h] = node;
    }

    public void remove(int key) {
        int h = hashFunc(key);
        Node head = list[h];
        Node prev = null;
        while (head != null) {
            if (head.key == key) {
                break;
            }
            prev = head;
            head = head.next;
        }
        if (head == null) { //Do nothing 
        }

        size--;
        if (prev != null) {
            prev.next = head.next;
        } else {
            list[h] = head.next;
        }
    }

}
